package lab2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void test_true() {
    Lab2 generator = new Lab2(100);
    assertTrue(generator.getSucsessful());
  }

  @Test void test_false(){
    Lab2 generator = new Lab2(1234567);
    generator.generate();
    assertEquals(generator.numsToString(), "превышено допустимое время работы программы, введите количество поменьше");
  }
}
