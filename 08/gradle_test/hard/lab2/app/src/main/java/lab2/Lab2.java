package lab2;

import java.util.Arrays;
/*
Пользователь вводит с клавиатуры количество чисел.
Программа должна случайным образом генерировать введенное
количество чисел и выводить время генерации. Если время
генерации больше 5 секунд, то выводить сообщение "превышено
допустимое время работы программы, введите количество поменьше".
Если время генерации меньше 5 секунд, то выводить
сгенерированные числа в консоль через запятую.
*/

public class Lab2 {

  private int[] nums;
  private long timeToGenerate;
  private int count;

  Lab2(int count){
    this.count = count;
    this.nums = new int[this.count];
  }

  public void generate(){

    long start = System.currentTimeMillis();

    for (int i = 0; i < this.count; i++){
      this.nums[i] = (int) (Math.random() * 100);
    }
    this.timeToGenerate = System.currentTimeMillis() - start;
  }

  public long getTimeToGenerate(){
    return this.timeToGenerate;
  }

  public boolean getSucsessful(){
    return (this.timeToGenerate <= 5);
  }

  public String numsToString(){
    if (this.getSucsessful()){
      return Arrays.toString(nums);
    }
    else{
      return "превышено допустимое время работы программы, введите количество поменьше";
    }
  }

}
