package mini;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest_lab1 {

  @Test void Fibo() {
    int[] arr2 = new int[] {1, 1, 2, 3, 5, 8};
    int[] arr1 = App_lab1.Fibo(6);
    assertArrayEquals(arr2, arr1);
  }

  @Test void Factorial() {
    int target = 6;
    int k = 3;
    int result = App_lab1.Factorial(k);
    assertEquals(target, result);
  }

  @Test void rFactorial() {
    int target = 6;
    int k = 3;
    int result = App_lab1.rFactorial(k);
    assertEquals(target, result);
  }

  @Test void summa() {
    int target = 6;
    int k = 3;
    int result = App_lab1.summa(k);
    assertEquals(target, result);
  }

  @Test void stroka() {
   int target = 7;
   String name = "aaaaaaabbbbbbbccccccdddddd";
   int result = App_lab1.stroka(name, 'a');
   assertEquals(target, result);
 }
}
