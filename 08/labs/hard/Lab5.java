package labs.hard;
import java.util.Scanner;
import java.util.Random;
/*
Создать программу, моделирующую процесс. С клавиатуры задается количество повторений,
размер области и сдвиг относительно начала координат. Заданное количество раз производится
генерация 4 случайных чисел
(1 - начало отрезка 1,
2 - конец отрезка 1,
3 - начало отрезка 2,
4 - конец отрезка 2). Программа должна выводить на консоль количество пересечений отрезков.
*/

public class Lab5{

  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);

    System.out.println("Введите количество повторений: ");
    int repeat = sc.nextInt();

    System.out.println("Введите размер области (конец): ");
    int area = sc.nextInt();

    System.out.println("Введите сдвиг относительно начала координат: ");
    int change = sc.nextInt();

    int i = 0;
    int counter = 0;
    while (i < repeat) {
      int first = change + new Random().nextInt(area);
      int second = change + new Random().nextInt(area);
      int third = change + new Random().nextInt(area);
      int fourth = change + new Random().nextInt(area);

      if ((first < third && third < second) || (first < fourth && fourth < second) ||
       (third < first && first < fourth) || (third < second && second < fourth)){
        counter++;
      }
      i++;
    }
    System.out.println(counter);
  }
}
