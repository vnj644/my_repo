package menu.Actions;

public class Action_4 extends Action{
  String title = "Вы находитесь в моей игре. Куда пойдете?";

  public Action_4(String title) {
    super(title);
  }

  public void act(){
    System.out.println("Здесь стоит домик, я думаю, вы можете там отдохнуть и спросить дорогу!"  + "\n");
  }
}
