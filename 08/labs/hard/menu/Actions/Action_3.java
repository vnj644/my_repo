package menu.Actions;

public class Action_3 extends Action{
  String title = "Вы находитесь в моей игре. Куда пойдете?";

  public Action_3(String title) {
    super(title);
  }

  public void act(){
    System.out.println("Тут пусто! Попробуйте вернуться и выбрать другую дорогу" + "\n");
  }
}
