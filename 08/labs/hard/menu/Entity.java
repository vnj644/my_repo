package menu;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;
import menu.Actions.*;

public class Entity{

  ArrayList<Entity> children = new ArrayList<Entity>();
  Entity parent;
  ArrayList<Action> actions = new ArrayList<Action>();

  public void addAction(Action action){
    this.actions.add(action);
  }

  public void nextAction(Entity nextLevel){
    this.children.add(nextLevel);
    nextLevel.parent = this;
  }

  public void userGame(){
    System.out.println("Выберите пункт:" + "\n");

    if (this.parent == null){
      System.out.println("0. Выход");
    }
    else{
      System.out.println("0. Выйти назад");
    }

    int num = 1;
    for (int i = 0; i < this.children.size(); i++) {
      System.out.println(num + ". " + "Следующий уровень");
      num++;
    }

    for (int i = 0; i < this.actions.size(); i++) {
      System.out.println(num + ". " + this.actions.get(i).getTitle());
      num++;
    }
    System.out.println();
  }
}
