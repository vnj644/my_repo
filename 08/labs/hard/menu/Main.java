package menu;

import java.util.Scanner;
import menu.Actions.*;

public class Main{
  public static void main(String[] args){

    Scanner sc = new Scanner(System.in);
    int user;

    Entity start = new Entity();
    Entity level1 = new Entity();
    Entity level1_2 = new Entity();
    Entity level2 = new Entity();
    Entity level2_2 = new Entity();

    Action one = new Action_1("Выбрать эту дорогу");

    start.nextAction(level1);
    start.nextAction(level1_2);
    start.addAction(one);

    level1.addAction(new Action_2("Выбрать эту дорогу"));
    level1.nextAction(level2);
    level1.nextAction(level2_2);

    level1_2.addAction(new Action_3("Выбрать эту дорогу"));

    level2.addAction(new Action_4("Выбрать эту дорогу"));

    level2_2.addAction(new Action_5("Выбрать эту дорогу"));

    while(true){
      start.userGame();
      user = sc.nextInt();
      System.out.println();

      if (user == 0){
        if (start.parent == null){
          System.out.println("Спасибо за игру!");
          break;
        }
        else start = start.parent;
      }
      if (user == 3){
        start.actions.get(user - 3).act();
      }
      if (user == 1 && start.children.size() == 0){
        start.actions.get(user - 1).act();
      }
      if(user > 0 && user < start.actions.size() + start.children.size()){
        start = start.children.get(user - start.children.size() + 1);
      }
    }
  }
}
