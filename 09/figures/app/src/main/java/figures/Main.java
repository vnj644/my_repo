package figures;
/*
Создать класс для описания многоугольников в общем и треугольника в частности.
Также создать класс для описания текста. Все классы должны иметь метод вывода
на экран. Создать отдельный класс для операций над объектами. В классе должен
быть метод вывода массива объектов на экран. Все объекты задавать в исходном
коде (не меньше 3-х различных треугольников и двух текстов).
 */

 public class Main{
   public static void main(String[] args){

    //  Polygon[] a = new Polygon[3];
    //  a[0] = new Triangle(8);
    //  a[1] = new Triangle(3);
    //  a[2] = new Triangle(12);

    //  Text t1 = new Text("Hi");
    //  Text t2 =  new Text("Wow");

    Output[] o = new Output[5];
    o[0] = new Triangle(8);
    o[1] = new Triangle(3);
    o[2] = new Triangle(12);
    o[3] = new Text("aaa");
    o[4] = new Text("bbb");

    Operations op = new Operations(o);
    op.outputPolygon();
    
   }
 }
