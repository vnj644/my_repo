package figures;
/*
Создать класс для описания многоугольников в общем и треугольника в частности.
Также создать класс для описания текста. Все классы должны иметь метод вывода
на экран. Создать отдельный класс для операций над объектами. В классе должен
быть метод вывода массива объектов на экран. Все объекты задавать в исходном
коде (не меньше 3-х различных треугольников и двух текстов).
 */
public class Operations{

  public Output[] figures;

  Operations(Output[] figures){
    this.figures = figures;
  }

  public void outputPolygon(){
    String[] arr = new String[this.figures.length];
    for (int i = 0; i < arr.length; i++) {
      System.out.println(this.figures[i].output());
    }
  }
}
