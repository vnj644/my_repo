package matrix;

/*
Создать программу, реализующую класс с операциями для работы с двумерными матрицами.
Класс должен позволять производить сложение, вычитание, умножение матриц. Предусмотреть
возможные ошибки и корректно их обрабатывать. Реализовать набор примеров для демонстрации
работы класса.
*/

public class Matrix {
  private int[][] matrix;
  private int n; // строка
  private int m; // столбец

  Matrix(int n, int m){
    this.n = n;
    this.m = m;
    this.matrix = new int[this.n][this.m];
    generate();
  }

  Matrix(){}

  public void setMatrix(int[][] matrix) throws MatrixException{
    if (matrix.length == this.n) {
      for (int i = 0; i < matrix.length; i++) {
        if (matrix[i].length != this.m) {
          throw new MatrixException("wrong matrix(1)");
        }
      }
    }
    else{
      throw new MatrixException("wrong matrix(2)");
    }

    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        this.matrix[i][j] = matrix[i][j];
      }
    }
  }

  public void generate(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        this.matrix[i][j] = (int) (Math.random() * 50);
      }
    }
  }

  public int[][] getMatrix(){
    return this.matrix;
  }
}
