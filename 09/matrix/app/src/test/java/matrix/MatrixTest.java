package matrix;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test void MatrixGeneration(){
        Matrix matrix = new Matrix(3, 3);
        int[][] result = matrix.getMatrix();
        assertEquals(result.length, 3);
    }

    @Test void MatrixGenerationFalse(){
        Matrix matrix = new Matrix(0, 0);
        int[][] result = matrix.getMatrix();
        assertEquals(result.length, 0);
    }
}
