package workers;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class AccountantTest{

  private Accountant ac1;
  private Accountant ac2;
  private Accountant ac3;

  @BeforeEach
  public void initialize(){
    ac1 = new Accountant("Jane", "accountant", 15000);
    ac2 = new Accountant("Jannie", "accountant", 15000);
    ac3 = new Accountant("Jannie", "accountant", 15000);
  }

  @Test void testName() {
    assertEquals("Jane", ac1.getName());
  }

  @Test void testProf() {
    assertEquals("accountant", ac1.getProf());
  }

  @Test void testSalary() {
    assertEquals(15000, ac1.getSalary());
  }

  @Test void testWork() {
    assertEquals("I am Accountant and I can count the budget", ac1.character_acc());
  }

  @Test void testNotEquals() {
    assertNotEquals(ac1, ac2);
  }

  @Test void testEquals() {
    assertEquals(ac2, ac3);
  }

  @Test void testHachCode() {
    assertNotEquals(ac2.hashCode(), ac1.hashCode());
  }
}
