package workers;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class CheifAccountantTest{

  private CheifAccountant cheif1;
  private CheifAccountant cheif2;

  @BeforeEach
  public void initialize(){
    cheif1 = new CheifAccountant("Max", "CheifAccountant", 30000);
    cheif2 = new CheifAccountant("Maks", "CheifAccountant", 30000);
  }

  @Test void testSalary() {
    assertEquals(30000, cheif1.getSalary());
  }

  @Test void testWork() {
    assertEquals("I am Accountant and I can count the budget even better than an ordinary Accountant!", cheif1.character_cheif());
  }

  @Test void testEquals() {
    assertEquals(false, cheif1.equals(cheif2));
  }

  @Test void testHachCode() {
    assertNotEquals(cheif2.hashCode(), cheif1.hashCode());
  }
}
