package vectors;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class AppTest {
  Vectors v1;
  Vectors v2;
  Vectors v3;

  @BeforeEach
  public void initalize(){
    v1 = new Vectors(15, 8, 3, 4);
    v2 = new Vectors(5, 6, 20, 11);
    v3 = new Vectors(5, 10, 15, 20);
  }

  @Test void StringVectors(){
    assertEquals("15.0 8.0 3.0 4.0", v1.toString());
  }

  @Test void Delta(){
    assertEquals(-12.0, v1.getDeltaX());
    assertEquals(-4.0, v1.getDeltaY());
    assertEquals(15.0, v2.getDeltaX());
    assertEquals(5.0, v2.getDeltaY());
  }

  @Test void additionV2(){
    Vectors new_v = Vectors.addition(v1, v2);
    assertEquals("15.0 8.0 18.0 9.0", new_v.toString());
  }

  @Test void subtractionV2(){
    Vectors new_v = Vectors.subtraction(v1, v2);
    assertEquals("15.0 8.0 -12.0 -1.0", new_v.toString());
  }

  @Test void multiplyScalar(){
    Vectors new_v = v3.multiplyScalar(2);
    assertEquals("5.0 10.0 30.0 40.0", new_v.toString());
  }

  @Test void divisionScalar(){
    Vectors new_v = v3.divisionScalar(5);
    assertEquals("5.0 10.0 3.0 4.0", new_v.toString());
  }


}
