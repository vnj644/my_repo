/*
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Создать массив из 30 000 строк длиной 10 случайных символов.
Найти, сумму значений отображения, соответствующих строкам из массива.
Запрещено создавать дополнительные структуры данных.
Сравнить скорость работы для разных базовых классов.
 */

package task2;

import java.util.Map;
import java.util.Random;

/**
* class for generating a map, an array, finding the sum
of values ​​that correspond to strings from an array
* @author Angelina Motrikala
* @version 1.0
* @see Main
 */

public class Maps{
  /** Field map */
  Map<String, Integer> map;
  /** Field stores string length */
  final static int LENGTH_KEY = 10;
  /** The number of characters that make up the string*/
  final static int CHAR_COUNT = 26;
  /** The size of the generated Map */
  final static int SIZE_MAP = 2000000;
  /** Maximum value for random generation*/
  final static int MAX_VALUE = 10000;
  /** Length of the generated array*/
  final static int LENGTH_ARRAY = 30000;
  /** Field to convert milliseconds to seconds*/
  final static int TIME_SEC = 1000;
  /** The number of times the average working time is calculated*/
  final static int AVERAGE_COUNT = 5;

  /**
  * construstor for maps
  * @param map
  */
  Maps(Map<String, Integer> map){
    this.map = map;
    valuesGeneration();
  }

  /**
  * method to generating a string with random characters
  * @return generated string(str)
  */
  public String strGeneration(){
    Random a = new Random();
    String str = "";
    for (int i = 0; i < LENGTH_KEY; i++) {
        str += (char) (a.nextInt(CHAR_COUNT) + 'a');
    }
    return str;
  }

  /**
  * method for generating map values
  */
  public void valuesGeneration(){
    double timestart = System.currentTimeMillis();

    for (int i = 0; i < SIZE_MAP; i++){
      int value = (int)(Math.random() * MAX_VALUE);
      this.map.put(strGeneration(), value);
    }

    double time = System.currentTimeMillis() - timestart;
    System.out.println("Random generation: " + time/TIME_SEC + " sec");
  }

  /**
  * method to generating array with random values
  * @return generated array
  */
  public String[] arrayGeneration(){
    String[] array = new String[LENGTH_ARRAY];
    for (int i = 0; i < LENGTH_ARRAY; i++){
      array[i] = strGeneration();
    }
    return array;
  }

  /**
  * method for finding the sum of mappings corresponding
  to strings from an array
  * @param array of string
  */
  public void sumValues(String[] array) {
    double timestart = System.currentTimeMillis();
    int sum = 0;

    for (int i = 0; i < LENGTH_ARRAY; i++){
      Integer val = this.map.get(array[i]);
      if (val != null) {
        sum += val;
      }
    }

    double time = System.currentTimeMillis() - timestart;
    System.out.println("Sum values: " + time/TIME_SEC + " sec");
    System.out.println("Sum: " + sum);
  }
  /**
  * method for calculating average values ​​of calculations
  */
  public void averageMap(){
    double time = 0;
    double averageMap = 0;

    for (int i = 0; i < AVERAGE_COUNT; i++){
      double timestart = System.currentTimeMillis();

      Maps m = new Maps(this.map);
      m.sumValues(m.arrayGeneration());

      time = System.currentTimeMillis() - timestart;
      System.out.println("All time: " + time/TIME_SEC + " sec");
    }
    averageMap = (time/AVERAGE_COUNT)/TIME_SEC;
    System.out.println("Среднее время у " + this.map.getClass() + " : " + averageMap + " sec");
  }
}
