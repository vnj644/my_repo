/*
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Создать массив из 30 000 строк длиной 10 случайных символов.
Найти, сумму значений отображения, соответствующих строкам из массива.
Запрещено создавать дополнительные структуры данных.
Сравнить скорость работы для разных базовых классов.
 */

package task2;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Hashtable;

public class Main{

  public static void main(String[] args){

    Maps hashmap = new Maps(new HashMap<String, Integer>());
    hashmap.averageMap();

    Maps treemap = new Maps(new TreeMap<String, Integer>());
    treemap.averageMap();

    Maps hashtable = new Maps(new Hashtable<String, Integer>());
    hashtable.averageMap();

    Maps linkedhashmap = new Maps(new LinkedHashMap<String, Integer>());
    linkedhashmap.averageMap();

  }
}
