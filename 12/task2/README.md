### Task

```
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Создать массив из 30 000 строк длиной 10 случайных символов.
Найти, сумму значений отображения, соответствующих строкам из массива.
Запрещено создавать дополнительные структуры данных.
Сравнить скорость работы для разных базовых классов.
```

### Diagram
[diagram](https://drive.google.com/file/d/1fZ8h9bF3zj80Ehb4lEE_3PohC66XIMZX/view?usp=sharing)

### Average time table
[table](https://docs.google.com/spreadsheets/d/16lCbofGCR9Szw4BruWeCe_o4gmajFXpmPqoCQJM81Hk/edit#gid=0)

### Benchmark
```
TestBenchmark.createHashMap        avgt    5  3,426 ± 0,129   s/op
TestBenchmark.createHashtable      avgt    5  3,393 ± 0,123   s/op
TestBenchmark.createLinkedHashMap  avgt    5  3,621 ± 0,526   s/op
TestBenchmark.createTreeMap        avgt    5  5,538 ± 0,490   s/op
TestBenchmark.sumHashMap           avgt    5  0,040 ± 0,001   s/op
TestBenchmark.sumHashtable         avgt    5  0,042 ± 0,001   s/op
TestBenchmark.sumLinkedHashMap     avgt    5  0,040 ± 0,003   s/op
TestBenchmark.sumTreeMap           avgt    5  0,071 ± 0,006   s/op
```
