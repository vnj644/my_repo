### Task

```
Предполагаем, что в тексте числительные идут всегда
перед существительным, к которому они относятся.
Вывести все существительные, о которых говорится, что они "пятые"
```

### Diagram
[Diagram](https://drive.google.com/file/d/1rOIgdAgrxkrZlQ1lhZR2X2jSHh4EmpzE/view?usp=sharing)
